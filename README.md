Hi guys

Thank you so much for your help with this.

Installation directions: 

1. 	Install the SA-MP 0.3 DL server (and client) from  http://forum.sa-mp.com/showthread.php?t=648633

2. 	Copy the "models", "filterscripts" and "plugins" folder into your SAMP server folder.

3. 	Add the "plugins streamer" line, or add "streamer" to your plugins line, in your server.cfg.

4. 	Run the game. Log in to RCON via /rcon login, and load the vc2sa4samp filterscript. You MAY need to delete your cache
	when you change models - you can find this in "Documents\GTA San Andreas User Files\SAMP\cache".
	
Once again thank you for your help. If you have any questions or queries, feel free to hit me up on Discord.

Kind regards,

Xalphox